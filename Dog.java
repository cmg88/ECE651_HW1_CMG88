public class Dog extends Animal{
    public Dog(String name){
	this.name = name;
	System.out.println("Dog " + name + " is born");
    }


    public String makeNoise(){
	return "Wang";
    }

    public String eat(){
	return "I am a dog and I eat dog food";
    }

    public boolean isPet(){
	return true;
    }

    public void roam(){
	System.out.println("Roamming around as a dog do");
    }


}