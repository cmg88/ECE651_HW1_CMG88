public class Cat extends Feline{
    public Cat(String name){
	this.name = name;
	System.out.println("Cat " + name + " is born");
    }

    public String makeNoise(){
	return "Meow";
    }

    public String eat(){
	return "I am a cat and I eat cat food";
    }

    public boolean isPet(){
	return true;
    }

    public void roam(){
	System.out.println("Roamming around as a cat do");
    }


}